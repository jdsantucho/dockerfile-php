Para crear la imagen con Dockerfile
docker build -t inventario .

Donde:
 docker build -t inventario <---- nombre de la imagen a crear
 
 . <---- Se carga el Dockerfile en el directorio donde nos encontremos

Para el despligue del contenedor con el sitio web utilizar la siguiente linea como ejemplo

docker run -d --name inventario-php -e APPSERVERNAME=inventario.php -e APPALIAS=www.inventario.php -e MYSQL_USER_PASSWORD=12345 -e MYSQL_USER=bduser -e MYSQL_DB_NAME=inventario -v /tmp/inventario/www:/var/www/html -p 8080:80 inventario


Descripcion 
docker run 


-d                              <---- Ejecutar contenedor como demonio

--name inventario-php           <---- Nombre del contenedor

-e APPSERVERNAME=inventario.php <---- Nombre del sitio para acceso (nginx) 

-e APPALIAS=www.inventario.php  <---- Alias del sitio para acceso (nginx)

-e MYSQL_USER_PASSWORD=12345    <---- contraseña para el usuario a crear

-e MYSQL_USER=bduser            <---- usuario a crear en la base de datos

-e MYSQL_DB_NAME=inventario     <---- nombre de la base de datos a crear

-v /tmp/inventario/www:/var/www/html  <---- Patch de trabajo

-p 8080:80                      <---- Redireccion de puerto

inventario                            <---- nombre de la imagen creada con Dockerfile


El archivo inventario.sql contiene toda la estructura de la base de datos (tablas, referencias, procedimientos almacenados)

